#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mydialog.h"


 MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->textEdit);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_Windows_triggered()
{
 //   mD = new MyDialog(this);

  // wersja 1
    /*
    MyDialog mDialog;           // w razie kliknięcia, tworze nowe okno/*
//    mDialog.setModal(true);     // nowe okno jest modalne, nie można klikać na inne gdy to jest aktywne. false niczego nie zmieni!!
    //mDialog.exec();
mDialog.show();
*/

    // wersja 2
    mD = new MyDialog(this);
    mD->show();
}

