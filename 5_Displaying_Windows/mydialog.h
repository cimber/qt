#ifndef MYDIALOG_H
#define MYDIALOG_H

#include <QDialog>
#include "mydialog.h"

namespace Ui {
class MyDialog;
}

class MyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MyDialog(QWidget *parent = 0);
    ~MyDialog();

private:
    Ui::MyDialog *ui;
//    MyDialog *mDialog;
};

#endif // MYDIALOG_H
