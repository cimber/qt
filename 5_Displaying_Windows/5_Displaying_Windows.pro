#-------------------------------------------------
#
# Project created by QtCreator 2016-12-08T22:08:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 5_Displaying_Windows
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mydialog.cpp

HEADERS  += mainwindow.h \
    mydialog.h

FORMS    += mainwindow.ui \
    mydialog.ui
