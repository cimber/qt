#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->horizontalSlider,           // obiekt z wejściem
            SIGNAL(valueChanged(int)),      //sygnał - rodzaj wejścia
            ui->progressBar,                //wyjście
            SLOT(setValue(int)));           //sposób procesowania na wyjściu

    connect(ui->horizontalSlider,           // obiekt z wejściem
            SIGNAL(valueChanged(int)),      //sygnał - rodzaj wejścia
            ui->progressBar_2,              //wyjście
            SLOT(setValue(int)));           //sposób procesowania na wyjściu

    disconnect(ui->horizontalSlider,        // obiekt z wejściem
            SIGNAL(valueChanged(int)),      //sygnał - rodzaj wejścia
            ui->progressBar,                //wyjście
            SLOT(setValue(int)));           //sposób procesowania na wyjściu

}

MainWindow::~MainWindow()
{
    delete ui;
}
